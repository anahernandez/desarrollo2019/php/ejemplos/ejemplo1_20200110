<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!--a php solo le interesa el name, el id lo estoy usando con el label
        para hacerlo accesible
        
        si quiero usar el mismo name le ponemos corchetes al final-->
        <form action="8.php" method="get" name="principal">
            <label for="inombre">Nombre del alumno</label>
            <input type="text" id="inombre" name="nombre">
            
            <label for="inumero1">Numero 1</label>
            <input type="number" id="inumero1" name="numero[]">
            
            <label for="inumero2">Numero 2</label>
             <input type="number" id="inumero2" name="numero[]">
             
             <div>
                 <label for="ipotes">Potes</label>
                 <input type="checkbox" id="ipotes" name="poblacion[]" value="Potes"/>
                 <label for="isantander">Santander</label>
                 <input type="checkbox" id="isantander" name="poblacion[]" value="Santander"/>       
             </div> 
             
             <!--Para radio buttons ponemos un mismo name porque solo 
             necesitamos que se envíe uno-->
             <div>                
                 <label for="iRojo">Rojo</label>
                 <input type="radio" id="iRojo" name="colores" value="Rojo"/>
                  <label for="iAzul">Azul</label>
                 <input type="radio" id="iAzul" name="colores" value="Azul"/>    
             </div>
             
             <!--Con múltiple te deja seleccionar varios a la vez
             entonces ponemos corchetes en el name para que mande array-->
             <div>
                 <label for="i nombres">Selecciona nombres</label>
                 <select name="nombres[]" id="inombres" multiple="true">
                     <option value="Roberto">Roberto</option>
                     <option value="Silvia">Silvia</option>
                     <option value="Laura">Laura</option>         
                 </select>
             </div>
             
             
            <button>Enviar</button>
            
        </form>
        <?php
        // put your code here
        ?>
    </body>
</html>
