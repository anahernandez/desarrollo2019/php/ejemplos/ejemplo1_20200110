<?php

//Inicializar variables al principio
/*Las variables no son objetos, son datos primitivos 
 * (no tienen propiedades ni métodos)*/
$contador=0;
var_dump($contador); //esto es como un console de javascript

$texto=null; //cuando no sabes qué va a contener
$texto=$texto . "a";
var_dump($texto);

$cadena="";
var_dump($cadena);

$cadena=10;
var_dump($cadena);

$cadena="Ejemplo";
$longitud=strlen($cadena);
var_dump($longitud);
