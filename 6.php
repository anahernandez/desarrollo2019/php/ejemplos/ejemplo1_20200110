<?php

$numeros=[
    1,4,5,6,7,8,9
];

//imprimir los números del array
//
//for
//imprimir notación corta en php
for($c=0;$c<count($numeros);$c++){
    ?>
<li><?= $c . ": " . $numeros[$c] ?></li> <!--Esta notación solo para imprimir-->
<?php
}
//imprimir notación "larga" en php
for($c=0;$c<count($numeros);$c++){
   //Si meto una variable dentro de comillas dobles saca el valor de la variable
echo "<li>$c:" . $numeros[$c] . "</li>";
}

//De esta forma me ahorro las comillasdel medio y concatenar
for($c=0;$c<count($numeros);$c++){
   //Si meto una variable dentro de comillas dobles saca el valor de la variable
echo "<li>$c: $numeros[$c]</li>";
}

//while
$c=0;
while($c<count($numeros)){
   
    echo "<li>$c: $numeros[$c]</li>";
     $c++;
}
//foreach
//En php se usa casi siempre foreach, for normal muy poco

// array seguido de as más una o dos variables
// primera variable indice del array
//segunda variable valor de cada posición
foreach ($numeros as $indice=>$valor) {
    echo "<li>$indice: $valor</li>";
}
